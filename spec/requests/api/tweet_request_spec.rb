require 'rails_helper'

RSpec.describe "API Tweets", type: :request do
  describe "#search" do
    let(:tweets) { create_list(:tweet, 10) }

    context 'search matches a tweet' do
      let(:search_query) { search_query }

      subject { get api_search_tweets_path(search: tweets[0].body) }

      it 'is successful' do
        expect(response).to have_http_status(:success)
      end

      it 'returns list of tweets that matches search query' do
        body = JSON.parse(response.body)

        expect(body).to eq([tweets[0].as_json])
      end
    end

    context 'search does not match a tweet' do
      let(:search_query) { 'no match' }

      subject { get api_search_tweets_path(search: search_query) }

      it 'is successful' do
        expect(response).to have_http_status(:not_found)
      end

      it 'returns list of tweets that matches search query' do
        body = JSON.parse(response.body)

        expect(body).to eq({ message: 'no tweets found for the search query: no match' })
      end
    end
  end

  describe "#create" do
    let(:response_body) { JSON.parse(response.body) }

    context 'with valid parameters' do
      let(:user1) { create(:user)}
      let(:valid_body) { 'This is a valid tweet' }

      it 'returns a successful response' do
        post api_tweets_path(user_id: user1.id, body: valid_body)

        expect(response).to have_http_status(:success)
      end

      it 'creates a new tweet' do
        expect {
          post api_tweets_path(user_id: user1.id, body: valid_body)
        }.to change(Tweet, :count).by(1)
      end
    end

    xcontext 'With invalid parameters' do
      let(:user1) { create(:user)}

      context 'When the body is too long' do
        let(:invalid_body) {  }

        it 'returns an error response' do

        end

        it 'does not create a new tweet' do

        end
      end

      context 'When the tweet might be a duplicate' do

      end
    end
  end
end
